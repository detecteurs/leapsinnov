#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "SiliconPlateConstruction.hh"
#include "CollimatorConstruction.hh"
#include "PrimaryGeneratorAction.hh"

#include "G4MaterialTable.hh"
#include "SoleilMaterials.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4Cons.hh"
#include "G4Torus.hh"
#include "G4Hype.hh"

#include "G4Transform3D.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4NistManager.hh"
#include <G4UserLimits.hh>

#include "G4MultiFunctionalDetector.hh"
#include "G4SDManager.hh"
#include "G4PSEnergyDeposit.hh"
#include <G4VPrimitiveScorer.hh>

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4VoxelLimits.hh"

#include "G4MTRunManager.hh"
#include "G4PhysicalConstants.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4Navigator.hh"
#include "G4TransportationManager.hh"

#include "G4GDMLParser.hh"

#include <G4VisAttributes.hh>
#include <iostream>
#include <fstream>
#include <iterator>

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*
Constructs DetectorConstruction, defines default values.
*/

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),  geDetectorBox(nullptr), 
logicCollimatorLEAPS(nullptr), 
logicSample(nullptr), 
geLogicVolume(nullptr) 
{
  fDetectorMessenger = new DetectorMessenger(this);
  halfSizeDarkBoxX = halfSizeDarkBoxY = halfSizeDarkBoxZ = 1.*m;
  fSetupName = "SAMBA";
  fDataType = "csv";
  halfDetectorLength = 11.5*mm;
  halfDetectorThickness = 2.0*mm;
  halfDetectorWidth = 11.5*mm;
  halfSampleLength = 5.0*mm;
  halfSampleThickness = 0.7*mm;
  halfSampleWidth = 5.0*mm;
  beWindowRadius = 20.0*mm;
  CollimatorThickness = 3.0*mm;
  distanceCollimatorDetector = 5.0*mm;
  distanceSampleWindow = 100.0*mm;
  ContactThickness = 800.0*nm;
  nSamples = 1;
  fDetectorType = 3;
  fDetectorName = "LEAPS_INNOV";
  fVolName = "World";
  materialConstruction = new SoleilMaterials;
  DefineMaterials();
  data_output_directory = "./";  
  fCollimatorMaterial = G4Material::GetMaterial("titanium_grade1");
  fTargetMaterial = G4Material::GetMaterial("G4_Ge");
  fSampleMaterial = G4Material::GetMaterial("EnviroMATNoCd");
  //fSampleMaterial = G4Material::GetMaterial("G4_POLYETHYLENE");
  fWorldMaterial = G4Material::GetMaterial("G4_Galactic");
  materialGeContainer = G4Material::GetMaterial("Aluminium");
  materialGeContainerCoating = G4Material::GetMaterial("Aluminium");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction(){
  //delete physicWorldBox;
  delete fDetectorMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::GetPhysicalVolumeByName(const G4String &name)
{
  // access the store of physical volumes
  G4PhysicalVolumeStore * pvs= G4PhysicalVolumeStore::GetInstance();
  G4VPhysicalVolume* pv;
  G4int npv= pvs->size();
  G4int ipv;
  for (ipv=0; ipv<npv; ipv++) {
    pv= (*pvs)[ipv];
    if (!pv)
      break;
    //G4cout<<" pv->GetName() "<<pv->GetName()<<G4endl;
    if (pv->GetName() == name)
      return pv;
  }
  return NULL;
}




/*
Sets which detector geometry is used.
*/
void DetectorConstruction::SetDetectorType(G4int value){
  fDetectorType=value;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  //G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}

void DetectorConstruction::SetNumberOfTargetSamples(G4int value){
  nSamples = value;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  //UpdateGeometry();
}

//Sets dimmensions of target, thickness corresponds to the Z coordinate, Length to x.
void DetectorConstruction::SetGeDetectorLength(G4double value){
  halfDetectorLength = (value/2.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}
void DetectorConstruction::SetGeDetectorThickness(G4double value){
  halfDetectorThickness = (value/2.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}
void DetectorConstruction::SetGeDetectorWidth(G4double value){
  halfDetectorWidth = (value/2.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

void DetectorConstruction::SetContactThickness(G4double value){
  ContactThickness = (value/1.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}
//Set sample dimmensions
void DetectorConstruction::SetSampleLength(G4double value){
  halfSampleLength = (value/2.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}
void DetectorConstruction::SetSampleThickness(G4double value){
  halfSampleThickness = (value/2.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}
void DetectorConstruction::SetSampleWidth(G4double value){
  halfSampleWidth = (value/2.)*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

void DetectorConstruction::SetBeWindowRadius(G4double value){
  beWindowRadius = value*mm;
  //UpdateGeometry();
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

//Collimator thickness
void DetectorConstruction::SetCollimatorThickness(G4double value){
  CollimatorThickness = value*mm;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

//Collimator to detector distance
void DetectorConstruction::SetDistanceCollimatorDetector(G4double value){
  distanceCollimatorDetector = value*mm;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

//Sample to window distance
void DetectorConstruction::SetDistanceSampleWindow(G4double value){
  distanceSampleWindow = value*mm;
  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

//Sets material of target.
void DetectorConstruction::SetTargetMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);

  if (pttoMaterial) {
    fTargetMaterial = pttoMaterial;
    if(geLogicVolume)geLogicVolume->SetMaterial(fTargetMaterial);
    G4cout<<" material: "<<fTargetMaterial->GetName()<<G4endl;  
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}

//Sets material of Ge Container
void DetectorConstruction::SetGeContainerMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);

  if (pttoMaterial) {
    materialGeContainer = pttoMaterial;
    if(logicGeContainer)logicGeContainer->SetMaterial(materialGeContainer);
    G4cout<<" material container: "<<materialGeContainer->GetName()<<G4endl;  
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}

//Sets material of Ge Container coating
void DetectorConstruction::SetGeContainerMaterialCoating(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);

  if (pttoMaterial) {
    materialGeContainerCoating = pttoMaterial;
    if(logicGeContainerCoating)logicGeContainerCoating->SetMaterial(materialGeContainerCoating);
    G4cout<<" material container coating: "<<materialGeContainerCoating->GetName()<<G4endl;  
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}

void DetectorConstruction::SetDetectorName(G4String detectorNAME)
{
	fDetectorName = detectorNAME;
}

void DetectorConstruction::SetSetupName(G4String setupNAME)
{
	fSetupName = setupNAME;
}
void DetectorConstruction::SetDataType(G4String dataType)
{
	fDataType = dataType;
}
/*
Sets material of sample.
*/
void DetectorConstruction::SetSampleMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);

  if (pttoMaterial) {
    fSampleMaterial = pttoMaterial;
    if(logicSample)logicSample->SetMaterial(fSampleMaterial);
    G4cout<<" material "<<fSampleMaterial->GetName()<<G4endl;  
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}



void DetectorConstruction::SetOutputDirectory(G4String output_directory)
{
    data_output_directory = output_directory;  
}
/*
Sets material of world volume.
*/
void DetectorConstruction::SetWorldMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
     G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);
  if (pttoMaterial) {
    fWorldMaterial = pttoMaterial;
    if ( logicWorldBox ) { logicWorldBox->SetMaterial(fWorldMaterial); }
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  //G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}

void DetectorConstruction::SetCollimatorMaterial(G4String materialChoice)
{
  // search the material by its name
  G4Material* pttoMaterial =
     G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);
  if (pttoMaterial) {
    fCollimatorMaterial = pttoMaterial;
    if ( logicCollimatorLEAPS ) { logicCollimatorLEAPS->SetMaterial(fCollimatorMaterial); }
  } else {
    G4cout << "\n--> warning from DetectorConstruction::SetMaterial : "
           << materialChoice << " not found" << G4endl;
  }
  G4RunManager::GetRunManager()->ReinitializeGeometry();
  //G4MTRunManager::GetRunManager()->PhysicsHasBeenModified();
}


/*
Defines materials used in simulation. Sets material properties for PEN and other optical components.
*/
void DetectorConstruction::DefineMaterials(){
  // ============================================================= Materials =============================================================
  //materialConstruction = new PenMaterials;
  materialConstruction-> Construct();
  materialAir = G4Material::GetMaterial("Air");
  materialBialkali = G4Material::GetMaterial("Bialkali");
  fGlass = G4Material::GetMaterial("BorosilicateGlass");
  PenMaterial = G4Material::GetMaterial("PEN");
  PVTMaterial = G4Material::GetMaterial("PVT_structure");
  materialSi = G4Material::GetMaterial("G4_Si");
  materialGe = G4Material::GetMaterial("G4_Ge");
  materialTriggerFoilEJ212 = G4Material::GetMaterial("EJ212");
  Pstyrene = G4Material::GetMaterial("Polystyrene");
  materialPMMA = G4Material::GetMaterial("PMMA");
  fVacuum = G4Material::GetMaterial("G4_Galactic");
  materialGreaseEJ550 = G4Material::GetMaterial("Grease");
  materialTeflon = G4Material::GetMaterial("G4_TEFLON");
  materialVikuiti = G4Material::GetMaterial("Vikuiti");
  materialTitanium = G4Material::GetMaterial("titanium_grade1");
  materialCollimatorCoating = G4Material::GetMaterial("Aluminium");
  materialSample = G4Material::GetMaterial("Iron");
  materialBeWindow = G4Material::GetMaterial("G4_Be");
  materialKaptonWindow = G4Material::GetMaterial("G4_KAPTON");
  materialMetalTube = G4Material::GetMaterial("G4_STAINLESS-STEEL");
  materialSupportPins = G4Material::GetMaterial("PEEK");
  materialContacts = G4Material::GetMaterial("Aluminium");
  materialPolyethylene = G4Material::GetMaterial("G4_POLYETHYLENE");

  G4cout<<" materials imported succesfully "<<G4endl;

}

void DetectorConstruction::SetVolName(G4ThreeVector thePoint){
  G4Navigator* theNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  G4VPhysicalVolume* myVolume= theNavigator->LocateGlobalPointAndSetup(thePoint);
  fVolName =  myVolume->GetName();
}


void DetectorConstruction::UpdateGeometry(){

  //define new one
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::Clean();
  G4LogicalVolumeStore::Clean();
  G4SolidStore::Clean();

  G4RunManager::GetRunManager()->ReinitializeGeometry();
}

/*
Clears stored geometry, then constructs all volumes that can be used in the simulation.

Builds and places volumes in world.

Defines detector sensitivities and properties.
*/
G4VPhysicalVolume* DetectorConstruction::Construct()
{
// ============================================================= Define Volumes =============================================================
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();
  G4LogicalSkinSurface::CleanSurfaceTable();
  G4LogicalBorderSurface::CleanSurfaceTable();
  //The experimental Dark Box
  fWorldBox = new G4Box("World",halfSizeDarkBoxX,halfSizeDarkBoxY,halfSizeDarkBoxZ);
  logicWorldBox = new G4LogicalVolume(fWorldBox,materialAir,"World",0,0,0);
  physicWorldBox = new G4PVPlacement(0,G4ThreeVector(),logicWorldBox,"World",0,false,0);

  geDetectorBox = new G4Box("target", halfDetectorLength, halfDetectorWidth, halfDetectorThickness);
  geLogicVolume = new G4LogicalVolume(geDetectorBox,fTargetMaterial, "target",0,0,0);


  AlContactBox = new G4Box("target", halfDetectorLength, halfDetectorWidth, ContactThickness/2.0);
  logicContactVolume = new G4LogicalVolume(AlContactBox, materialContacts,
                                      "Contact", 0, 0, 0);
  
  SampleBox = new G4Box("sample", halfSampleLength, halfSampleWidth, halfSampleThickness);
  logicSample = new G4LogicalVolume(SampleBox,fSampleMaterial, "sample",0,0,0);

  G4RotationMatrix* rotationMatrixSample = new G4RotationMatrix();
  rotationMatrixSample->rotateY(45.*deg);

  //gdml files pen holders
  //fParser.Write("../input_files/PEN-018695-01Dec20_1644491893.ast.gdml",physicWorldBox);

  //comments this part on 02/03/2023
  //G4ThreeVector point = G4ThreeVector(0,0,5*cm);
  //G4Navigator* pointNavigator = new G4Navigator();
  //pointNavigator->SetWorldVolume(physicWorldBox);
  //pointNavigator->LocateGlobalPointAndSetup(point);

  //Copper/Al inner detector container
  G4double geContainerInnerRadius = 27.0*mm;
  G4double geContainerOuterRadius = 32.0*mm;
  G4double geContainerHalfThickness = halfDetectorThickness*2 + 1.0*mm;
  G4double geContainerCoatingInnerRadius = geContainerInnerRadius - 100.0*um;

  G4double coating_thickness = 100.0*um;
  G4double spaceBetweenVolumes = 0.5*mm;
  G4double beWindowThickness = 0.5*0.125*mm;
  G4double metalWindowThickness = 1.0*mm;
  G4double metalWindowRadius = 35.0*mm;
  G4double metalTubeExternalRadius = 40.0*mm;
  G4double peek_support_pins_Thickness = 0.5*1.0*mm;
  G4double halfCapotThickness = 1.0*mm;
  //Placement collimator and others
  G4double zPositionContacts = halfDetectorThickness + ContactThickness/2.0;
  G4double zPositionCollimator =  CollimatorThickness/2.0 + halfDetectorThickness + ContactThickness + distanceCollimatorDetector;
  G4double zPositionBeWindow =  zPositionCollimator + CollimatorThickness/2.0 + metalWindowThickness/2.0 + 2.0*halfCapotThickness + 20*spaceBetweenVolumes;
  G4double zPositionKaptonWindow =  zPositionBeWindow + 4.*mm;
  zPositionSample =  zPositionBeWindow + distanceSampleWindow;
  G4double zPositionSupportPins = geContainerHalfThickness + 5*spaceBetweenVolumes + peek_support_pins_Thickness;
  G4double zPositionCapot = zPositionCollimator + CollimatorThickness/2.0 + 4*spaceBetweenVolumes;
  G4cout<<" z position of collimator "<<zPositionCollimator<<G4endl;

  /*
  //for debuging purposes to identify particle polarization when rotated wrt global frame
  G4Tubs* geDetectorTube = new G4Tubs("target", 25.*mm, 30.*mm, 10.*mm, 0.*deg, 360.*deg);
  G4LogicalVolume* geLogicVolumeRing = new G4LogicalVolume(geDetectorTube,fTargetMaterial, "target",0,0,0);

  G4Tubs* geDetectorTubeSample = new G4Tubs("sample", 0., 1.5*mm, 3.*mm, 0.*deg, 360.*deg);
  G4LogicalVolume* geLogicVolumeRingSample = new G4LogicalVolume(geDetectorTubeSample,fSampleMaterial, "target",0,0,0);

  G4RotationMatrix* rotationMatrixTube = new G4RotationMatrix(0,0,0);
  rotationMatrixTube->rotateY(90*deg);
  //end of debuging
  */

  if (fDetectorType == 0 || fDetectorType == 2){
  	zPositionBeWindow =  zPositionCollimator - CollimatorThickness/2.0 + metalWindowThickness/2.0 + 2.0*halfCapotThickness + 20*spaceBetweenVolumes;
        zPositionKaptonWindow =  zPositionBeWindow + 4.*mm;
        zPositionSample =  zPositionBeWindow + distanceSampleWindow;
        zPositionCapot = geContainerHalfThickness + 4*spaceBetweenVolumes; 
  }

  G4double metalTubeLength = zPositionBeWindow + metalWindowThickness;

  G4ThreeVector positionWindow(0.*mm, 0.*mm, zPositionBeWindow);
  G4ThreeVector positionKaptonWindow(0.*mm, 0.*mm, zPositionKaptonWindow);
  G4ThreeVector positionWindowBack(0.*mm, 0.*mm, -1*zPositionBeWindow);
  G4ThreeVector positionSample(0.*mm, 0.*mm, zPositionSample);
  
  //ge container
  solidGeContainer = new G4Tubs("ge_container", geContainerInnerRadius, geContainerOuterRadius, geContainerHalfThickness, 0.*deg,360.*deg);
  solidGeContainerCoating = new G4Tubs("ge_container_coating", geContainerCoatingInnerRadius, geContainerInnerRadius, geContainerHalfThickness, 0.*deg,360.*deg);

  logicGeContainer = new G4LogicalVolume(solidGeContainer, materialGeContainer,"geContainer", 0, 0, 0);
  logicGeContainerCoating = new G4LogicalVolume(solidGeContainerCoating, materialGeContainerCoating,"geContainer", 0, 0, 0);

  //Support Pins
  solidSupportPins = new G4Tubs("peek_support_pins", 0.0*mm, geContainerOuterRadius, peek_support_pins_Thickness, 0.*deg,360.*deg);
  logicSupportPins = new G4LogicalVolume(solidSupportPins, materialSupportPins,"SupportPins", 0, 0, 0);

  //front <<capot>>
  solidGeCapotPrimitive = new G4Tubs("ge_capot_alu_primitive", 0.0*mm, geContainerOuterRadius, halfCapotThickness, 0.*deg, 360.*deg);
  solidBoxCapotInner = new G4Box("capot_inner", halfDetectorLength, halfDetectorWidth, 2.0*halfCapotThickness);
  solidGeCapot = new G4SubtractionSolid("ge_capot", solidGeCapotPrimitive, solidBoxCapotInner, 0,  G4ThreeVector(0.,0.,0.));

  logicGeCapot = new G4LogicalVolume(solidGeCapot, materialContacts,"logic_capot", 0, 0, 0);


  //Kapton in front assuming same thickness as Be window
  solidKaptonWindow = new G4Tubs("kaptonwindow",0.0*mm, beWindowRadius, beWindowThickness, 0.*deg, 360.*deg);
  logicKaptonWindow = new G4LogicalVolume(solidKaptonWindow, materialKaptonWindow,"kaptonWindow", 0, 0, 0);
  // Berilium window.
  solidBeWindow = new G4Tubs("window",0.0*mm, beWindowRadius, beWindowThickness, 0.*deg, 360.*deg);
  logicBeWindow = new G4LogicalVolume(solidBeWindow, materialBeWindow,"Window", 0, 0, 0);

  //Front window
  solidMetalWindow = new G4Tubs("window", beWindowRadius, metalWindowRadius, metalWindowThickness, 0.*deg, 360.*deg);
  logicMetalWindow = new G4LogicalVolume(solidMetalWindow, materialMetalTube,"Window", 0, 0, 0);

  //Back part
  solidMetalWindowBack = new G4Tubs("window", 0.0*mm, metalWindowRadius, metalWindowThickness, 0.*deg, 360.*deg);
  logicMetalWindowBack = new G4LogicalVolume(solidMetalWindowBack, materialMetalTube,"Window", 0, 0, 0);

  //Tube body
  solidMetalTube = new G4Tubs("window", metalWindowRadius, metalTubeExternalRadius, metalTubeLength, 0.*deg, 360.*deg);
  logicMetalTube = new G4LogicalVolume(solidMetalTube, materialMetalTube,"Window", 0, 0, 0);
  // ============================================================= Detectors =============================================================
  CollimatorConstruction myCollimator;
  G4VSolid* CollimatorLEAPSCoating = myCollimator.ConstructCollimatorCoating(CollimatorThickness, coating_thickness);
  G4VSolid* CollimatorLEAPS = myCollimator.ConstructCollimator(CollimatorThickness, coating_thickness);
  G4VSolid* CollimatorLEAPSSmall = myCollimator.ConstructSmallCollimator(CollimatorThickness, coating_thickness);
  
  //SAMBA collimator
  G4VSolid* CollimatorSAMBA = myCollimator.ConstructSAMBACollimator(CollimatorThickness);

  //G4double halfCollimatorSizeX = myCollimator.GetCollimatorSizeX();
  //G4double halfCollimatorSizeY = myCollimator.GetCollimatorSizeY();
  //G4double halfCollimatorSizeZ = myCollimator.GetCollimatorSizeZ();
  G4cout<<" myCollimator size x "<<myCollimator.GetCollimatorSizeX()<<G4endl;

  
  logicCollimatorLEAPS = new G4LogicalVolume(CollimatorLEAPS, fCollimatorMaterial, "log_collimator");
  logicCollimatorLEAPSSmall = new G4LogicalVolume(CollimatorLEAPSSmall, fCollimatorMaterial, "log_collimator_small");
  logicCollimatorLEAPSCoating = new G4LogicalVolume(CollimatorLEAPSCoating, materialCollimatorCoating, "log_collimator_coating");
  
  logicCollimatorSAMBA = new G4LogicalVolume(CollimatorSAMBA, fCollimatorMaterial, "log_collimator_samba");
  // Set Draw G4VisAttributes
  G4VisAttributes* visAttr = new G4VisAttributes();
  visAttr->SetVisibility(false);
  logicWorldBox->SetVisAttributes(visAttr);

  //Detector
  G4VisAttributes* visualAttributesGeDetector = new G4VisAttributes(G4Colour::Blue());
  visualAttributesGeDetector->SetVisibility(true);
  visualAttributesGeDetector->SetForceWireframe(true);
  visualAttributesGeDetector->SetForceAuxEdgeVisible(true);
  visualAttributesGeDetector->SetForceSolid(true);
  geLogicVolume->SetVisAttributes(visualAttributesGeDetector);
  //geLogicVolumeRing->SetVisAttributes(visualAttributesGeDetector);
  //geLogicVolumeRingSample->SetVisAttributes(visualAttributesGeDetector);

  //container attributes 
  G4VisAttributes* visualAttributesContainer = new G4VisAttributes(G4Colour::Brown());
  visualAttributesContainer->SetVisibility(true);
  visualAttributesContainer->SetForceWireframe(true);
  visualAttributesContainer->SetForceAuxEdgeVisible(true);
  visualAttributesContainer->SetForceSolid(true);
  logicGeContainer->SetVisAttributes(visualAttributesContainer);

  G4VisAttributes* visualAttributesGeDetectorContacts = new G4VisAttributes(G4Colour::Black());
  visualAttributesGeDetectorContacts->SetVisibility(true);
  visualAttributesGeDetectorContacts->SetForceWireframe(true);
  visualAttributesGeDetectorContacts->SetForceAuxEdgeVisible(true);
  visualAttributesGeDetectorContacts->SetForceSolid(true);
  logicContactVolume->SetVisAttributes(visualAttributesGeDetectorContacts);

  G4VisAttributes* visualAttributesWindow = new G4VisAttributes(G4Colour::Yellow());
  visualAttributesWindow->SetVisibility(true);
  visualAttributesWindow->SetForceWireframe(true);
  visualAttributesWindow->SetForceAuxEdgeVisible(true);
  logicBeWindow->SetVisAttributes(visualAttributesWindow);
  logicKaptonWindow->SetVisAttributes(visualAttributesWindow);

  G4VisAttributes* visualAttributesSample = new G4VisAttributes(G4Colour::Cyan());
  visualAttributesSample->SetVisibility(true);
  visualAttributesSample->SetForceSolid(true);
  logicSample->SetVisAttributes(visualAttributesSample);

  G4VisAttributes* visualAttributesCollimator = new G4VisAttributes(G4Colour::Magenta());
  visualAttributesCollimator->SetVisibility(true);
  visualAttributesCollimator->SetForceWireframe(true);
  visualAttributesCollimator->SetLineWidth(2);
  visualAttributesCollimator->SetForceSolid(true);
  logicCollimatorLEAPS->SetVisAttributes(visualAttributesCollimator);
  logicCollimatorLEAPSSmall->SetVisAttributes(visualAttributesCollimator);


  G4VisAttributes* visualAttributesPEEK = new G4VisAttributes(G4Colour::Cyan());
  visualAttributesPEEK->SetVisibility(true);
  visualAttributesPEEK->SetForceWireframe(true);
  visualAttributesPEEK->SetForceAuxEdgeVisible(true);
  logicSupportPins->SetVisAttributes(visualAttributesPEEK);
  logicGeCapot->SetVisAttributes(visualAttributesPEEK);


  G4VisAttributes* visualAttributesCollimatorCoating = new G4VisAttributes(G4Colour::Brown());
  visualAttributesCollimatorCoating->SetVisibility(true);
  visualAttributesCollimatorCoating->SetForceWireframe(true);
  visualAttributesCollimatorCoating->SetLineWidth(3);
  //visualAttributesCollimatorCoating->SetForceAuxEdgeVisible(true);
  //visualAttributesCollimatorCoating->SetForceSolid(true);
  logicCollimatorLEAPSCoating->SetVisAttributes(visualAttributesCollimatorCoating);
  logicCollimatorSAMBA->SetVisAttributes(visualAttributesCollimatorCoating);

  G4VisAttributes* visualAttributesTubeWindow = new G4VisAttributes(G4Colour::Gray());
  visualAttributesTubeWindow->SetVisibility(true);
  visualAttributesTubeWindow->SetForceSolid(true);
  visualAttributesTubeWindow->SetForceAuxEdgeVisible(true);
  logicGeContainerCoating->SetVisAttributes(visualAttributesTubeWindow);
  
  G4VisAttributes* visualAttributesTubeBody = new G4VisAttributes(G4Colour::Gray());
  visualAttributesTubeBody->SetVisibility(true);
  //visualAttributesTubeWindow->SetForceSolid(true);
  visualAttributesTubeBody->SetForceAuxEdgeVisible(true);
  logicMetalTube->SetVisAttributes(visualAttributesTubeBody);
  logicMetalWindowBack->SetVisAttributes(visualAttributesTubeBody);
  logicMetalWindow->SetVisAttributes(visualAttributesTubeBody);
  


  //  ============================================================= Place volumes =============================================================
  
  // Place main detector always at center of world volume
  switch(fDetectorType){

  //Detector alone for "direct" beam radiation	  
  case 0:
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		geLogicVolume,
		"target_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
	 	logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);
    
     break;
  //Detector + collimator for "direct" beam radiation
  case 1:
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		geLogicVolume,
		"target_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorLEAPSCoating,
	        "CollimCoating", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorLEAPS,
	        "Collim", 
		logicWorldBox, false,1,false);
 
     break;
   //Detector + sample
   case 2:
  	  new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		geLogicVolume,
		"target_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
		logicWorldBox, false,1,false);
 
         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(rotationMatrixSample, 
	        positionSample, 
	        logicSample,
	        "Sample", 
		logicWorldBox, false,1,false);
 
     break;
   //Detector + sample + collimator
   case 3:
          new G4PVPlacement(0,
                G4ThreeVector(0,0,0),
                geLogicVolume,
                "target_"+std::to_string(1),
                logicWorldBox,false,1,false); 

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorLEAPSCoating,
	        "CollimCoating", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorLEAPS,
	        "Collim", 
		logicWorldBox, false,1,false);
 
         new G4PVPlacement(rotationMatrixSample, 
	        positionSample, 
	        logicSample,
	        "Sample", 
		logicWorldBox, false,1,false);
     break;

    case 4:
    //Detector + sample + small collimator
    //
    new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		geLogicVolume,
		"target_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);
         
	 new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorLEAPSSmall,
	        "Collim", 
		logicWorldBox, false,1,false);
 
         new G4PVPlacement(rotationMatrixSample, 
	        positionSample, 
	        logicSample,
	        "Sample", 
		logicWorldBox, false,1,false);
 
     break;
     //SAMBA Detector + collimator for "direct" beam radiation
     case 5:
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		geLogicVolume,
		"target_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorSAMBA,
	        "Collim", 
		logicWorldBox, false,1,false);

 
     break;
   //SAMBA Detector + collimator + sample for beam radiation on sample
     case 6:
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		geLogicVolume,
		"target_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainer,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,0),
		logicGeContainerCoating,
		"container_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionSupportPins),
		logicSupportPins,
		"support_pins_"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionCapot),
		logicGeCapot,
		"ge_capot"+std::to_string(1),
		logicWorldBox,false,1,false);
 
  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

  	 new G4PVPlacement(0, 
		G4ThreeVector(0,0,-zPositionContacts),
		logicContactVolume,
		"contact_"+std::to_string(1),
		logicWorldBox,false,1,false);

         new G4PVPlacement(0, 
	        positionWindow, 
	        logicBeWindow,
	        "Window", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionKaptonWindow, 
	        logicKaptonWindow,
	        "Window", 
	 	logicWorldBox, false,1,false);
        
         new G4PVPlacement(0, 
	        positionWindow, 
	        logicMetalWindow,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        positionWindowBack, 
	        logicMetalWindowBack,
	        "WindowAl", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,0), 
	        logicMetalTube,
	        "metalTube", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(0, 
	        G4ThreeVector(0,0,zPositionCollimator), 
	        logicCollimatorSAMBA,
	        "Collim", 
		logicWorldBox, false,1,false);

         new G4PVPlacement(rotationMatrixSample, 
	        positionSample, 
	        logicSample,
	        "Sample", 
		logicWorldBox, false,1,false);
         /*
         new G4PVPlacement(rotationMatrixTube, 
	        positionSample, 
	        geLogicVolumeRing,
	        "target_1", 
		logicWorldBox, false,1,false);
	 */
 
     break;

  }

  //// Definition of simulation steps.
  //logicWorldBox->SetUserLimits(new G4UserLimits(0.1*mm));
  //geLogicVolume->SetUserLimits(new G4UserLimits(1.0*um));
  //logicBeWindow->SetUserLimits(new G4UserLimits(50.0*um));
  //logicContactVolume->SetUserLimits(new G4UserLimits(5.0*nm));

      
  return physicWorldBox;
}
