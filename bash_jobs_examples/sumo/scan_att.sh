#!/bin/sh
for thick in 0.3 0.5 1.0 3.0 5.0 
do
        echo $thick
	for job in {1..5..1}
	do
		for energy in 5 10 20 30 40 50 60 70 80 90 100  
		do
			for z_source in 300
			do
				FILE="run_E_${energy}_collimator_${z_source}_thickness_${thick}mm_job_${job}.mac"

			 	/bin/cat <<EOM >$FILE
/process/em/fluo true
/process/em/auger true
/process/em/augerCascade true
/process/em/pixe true

/Ge2SOS/det/setOutputDirectory /lustre/work/experiences/detecteurs/manzanillas/LEAPS_INNOV/Geant4_output/tungsten_lines/
#select detector type: options are 0: detector without collimator and direct beam, 1 detector with collimator and direct beam, 2 and 3 same as 0 and 1 but with sample and beam on sample
/Ge2SOS/det/setDetectorType 1
/Ge2SOS/det/setSetupName ta_thickness_${thick}mm_run_${job}
#select output format, options are: csv root hdf5
/Ge2SOS/det/setDataType csv
/Ge2SOS/det/setNTargetSamples 1
/Ge2SOS/det/setGeDetectorThickness 3.96 mm
/Ge2SOS/det/setGeDetectorWidth 24.9 mm
/Ge2SOS/det/setGeDetectorLength 24.9 mm
#container material Aluminium  + Aluminium or G4_Cu + G4_Au
/Ge2SOS/det/setGeContainerMat G4_Al
/Ge2SOS/det/setGeContainerMatCoating G4_Al
/Ge2SOS/det/setCollimatorThickness ${thick} mm
#/Ge2SOS/det/setDistanceCollimatorDetector 1. mm
#select material of the collimator, recomended options: titanium_grade1, titanium_grade2, tungsten_alloy, G4_BRASS, G4_Pb, G4_Ta
/Ge2SOS/det/setCollimatorMat G4_Ta
/Ge2SOS/det/setBeWindowRadius 18.0 mm
#Set the position of the collimator, a values between -37 and 37 mm (size of samples)
#Choise of source type: 0 gamma,1 Fe-55,2 Cs-137; 1 Bi-207; 2 Sr-90; 3 e-
/run/initialize
/Ge2SOS/gun/sourceType 0
#/Ge2SOS/gun/sourceDirectionType 1
/Ge2SOS/gun/sourceDiameter 30.0 mm
/Ge2SOS/gun/sourcePositionZ ${z_source}.0 mm
/Ge2SOS/gun/sourceEnergy ${energy}.0  keV

/run/beamOn 4000000
EOM
			FILEJOB="job_E_${energy}_${thick}mm_z_${z_source}_job_${job}.sh"

                	/bin/cat <<EOM >$FILEJOB
#!/bin/bash
#SBATCH -n 1
#SBATCH --qos=parallel
#SBATCH --partition=sumo
#SBATCH --time=2:00:00
#SBATCH --cpus-per-task=2
#
source ~/environment_GeSOS.sh
./Ge2SOS -m $FILE  
exit 0
EOM
sbatch $FILEJOB
			done
		done

	done
done

































