#!/bin/sh
polarization_percent=90
z_sample=50
for angle_polarization in 90
do
        echo $angle_polarization
	for job in {1..100..1}
	do
		for energy in 32 
		do
			FILE="run_E_${energy}_z_${z_sample}_samba_pol_${polarization_percent}_pc_angle_${angle_polarization}_job_${job}.mac"

			/bin/cat <<EOM >$FILE
/process/em/fluo true
/process/em/auger true
/process/em/augerCascade true
/process/em/pixe true

/Ge2SOS/det/setOutputDirectory  /ccc/work/cont003/soleil/manzanil/EnviroMAT/Geant4_output/
#select detector type: options are 0: detector without collimator and direct beam, 1 detector with collimator and direct beam, 2 and 3 same as 0 and 1 but with sample and beam on sample
/Ge2SOS/det/setDetectorType 6
/Ge2SOS/det/setSetupName enviroMATNoCd_z_${z_sample}mm_pol_${angle_polarization}d_${polarization_percent}pc_run_${job}
#select output format, options are: csv root hdf5
/Ge2SOS/det/setDataType csv
/Ge2SOS/det/setNTargetSamples 1
/Ge2SOS/det/setSampleMaterial EnviroMATCd100ppm
/Ge2SOS/det/setGeDetectorThickness 6.99 mm
/Ge2SOS/det/setGeDetectorLength 36. mm
/Ge2SOS/det/setGeDetectorWidth 36. mm
/Ge2SOS/det/setCollimatorThickness 1. mm
/Ge2SOS/det/setCollimatorMat titanium_grade1
/Ge2SOS/det/setDistanceSampleWindow ${z_sample}. mm
/Ge2SOS/det/setBeWindowRadius 26.0 mm
#Set the position of the collimator, a values between -37 and 37 mm (size of samples)
#Choise of source type: 0 gamma,1 Fe-55,2 Cs-137; 1 Bi-207; 2 Sr-90; 3 e-

/run/initialize
/Ge2SOS/gun/sourceType 0
/Ge2SOS/gun/sourceDiameter 0.3 mm
/Ge2SOS/gun/sourceEnergy ${energy}.0  keV
/Ge2SOS/gun/sourceGammaPolarizationAngle ${angle_polarization}.0
/Ge2SOS/gun/sourceGammaPolarizationDegree 0.${polarization_percent}

/run/beamOn 900000000

EOM
			FILEJOB="job_E_${energy}_${angle_polarization}mm_z_${z_sample}_${polarization_percent}_job_${job}.sh"

                	/bin/cat <<EOM >$FILEJOB
#!/bin/bash
#MSUB -r MyJob_case_${job} # Request name
#MSUB -n 1 # Total number of processes to use
#MSUB -c 1 # Number of cores per process to use
#MSUB -T 80000 # Elapsed time limit in seconds
#MSUB -o /ccc/work/cont003/soleil/manzanil/reconstruction/capa_diamond_%I.o # Standard output. %I is the job id
#MSUB -e /ccc/work/cont003/soleil/manzanil/reconstruction/capa_diamond_%I.e # Error output. %I is the job id
#MSUB -q milan # Choosing standard nodes
#MSUB -m work, scratch # Mount files systems
export OMP_NUM_THREADS=6
source /ccc/cont003/home/soleil/manzanil/setup_geant4.sh
ccc_mprun ./Ge2SOS -m ${FILE}
exit 0
EOM
ccc_msub $FILEJOB
		done

	done
done

































