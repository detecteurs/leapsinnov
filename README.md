# GeSOS Geant4 software
GEANT4 software to simulate interaction of X rays with setup containing a HPGe Ge detector.

It can simulate four different setups that will be used to study interactions of x-rays in different configurations. 

# Compilation in SUMO 

In order to compile the software you need to charge root and Geant4(v4.11). In SUMO you need to do:

```
## Modules:
module load physics/geant4/4.11
module load tools/gcc/9.2.0
## Environment variables for Geant4 and ROOT.
export GEANT4=/usr/local/applications/physics/geant4/4.11/
export ROOT6=/usr/local/applications/tools/root6/
export BOOST=/usr/local/applications/tools/boost/1.78
## Geant4 and ROOT environment variables are loaded.
source $GEANT4/bin/geant4.sh
source $ROOT6/bin/thisroot.sh
## libraries. Not sure if this line is realle needed, but include it, just in case
export LD_LIBRARY_PATH=$BOOST/lib:$LD_LIBRARY_PATH:$ROOT6/lib:$GEANT4/lib64:/usr/lib64/:/usr/lib
```
It's recommended to copy these instruction is a setup_geant4.sh file and then just call `source setup_geant4.sh` when you start a new session in sumo 

Then clone the repository in your local machine
Then once the software is in your local repository, cd your_repository and follow these intructions
1) create a build folder: ```mkdir build``` (if the folder exist you should clean it, ```cd build```, ```rm -rf *```)
2) ```cmake ../``` or replace ../ by the location of the software
3) ```make```
This should create the GeSOS executable. Then to run it in interactive mode, just do inside build: ```./GeSOS```

# Compilation in TOPAZE

In order to compile the software in topaze you just need to copy the repository to topaze and charge root and Geant4(v4.11). In TOPAZE you need to do:

```
# SOLEIL bashrc
source /ccc/cont003/home/solcomm/solcomm/etc/.bashrc_soleil

# ROOT6
module load soleil/topaze/root6
source $ROOT6/bin/thisroot.sh

# GEANT4
module load soleil/topaze/geant4
source $GEANT4/bin/geant4.sh

```

It's recommended to copy these instruction is a setup_geant4.sh file and then just call `source setup_geant4.sh` when you start a new session in topaze 

Then copy to topaze the repository with the source code using scp. 
Once the software is in your local repository, cd your_repository and follow these intructions
1) create a build folder: ```mkdir build``` (if the folder exist you should clean it, ```cd build```, ```rm -rf *```)
2) ```cmake ../``` or replace ../ by the location of the software
3) ```make```
This should create the GeSOS executable. Then to run it in interactive mode, just do inside build: ```./GeSOS```


# Simulation setup 

Select the setup that you want to simulate with:

``` /GeSOS/det/setDetectorType 1```
Options are:

setup `0` consist of Ge detector without collimator for direct x-ray beam radiation: `/GeSOS/det/setDetectorType 0`

![image info](figures/Ge2SOS_det0.PNG)

setup `1` consist of Ge detector with collimator for direct x-ray beam radiation: `/GeSOS/det/setDetectorType 1`

![image info](figures/Ge2SOS_det1.PNG)

setup `2` consist of Ge detector without collimator plus a sample for direct x-ray beam on sample: `/GeSOS/det/setDetectorType 2`

![image info](figures/Ge2SOS_det2.PNG)

setup `3` consist of Ge detector with collimator plus a sample for direct x-ray beam on sample: `/GeSOS/det/setDetectorType 3`
![image info](figures/Ge2SOS_det3.PNG)

setup `4` consist of Ge detector with small collimator plus a sample for direct x-ray beam on sample: `/GeSOS/det/setDetectorType 4`

![image info](figures/Ge2SOS_det4.PNG)

setup `5` consist of SAMBA Ge detector geometry including collimator for direct x-ray beam radiation: `/GeSOS/det/setDetectorType 5`

![image info](figures/Ge2SOS_det5.PNG)

setup `6` consist of SAMBA Ge detector geometry including collimator + a saple for direct x-ray beam on sample: `/GeSOS/det/setDetectorType 6`

![image info](figures/Ge2SOS_det6.PNG)


Once you have selected the type of setup that you want to simulate you can also choose the material and thicknees of the collimator (if any), etc 

An example of config file my_setup.mac is hte following


```
/process/em/fluo true
/process/em/auger true
/process/em/augerCascade true
/process/em/pixe true

/Ge2SOS/det/setOutputDirectory  /ccc/work/cont003/soleil/manzanil/EnviroMAT/Geant4_output/
/Ge2SOS/det/setDetectorType 3
/Ge2SOS/det/setSetupName leaps_BP_EnviroMATCd10ppm_z_50mm_pol_90d_99pc_run_400
#select output format, options are: csv root hdf5
/Ge2SOS/det/setDataType csv
/Ge2SOS/det/setNTargetSamples 1
/Ge2SOS/det/setSampleMaterial EnviroMATCd10ppm
/Ge2SOS/det/setGeDetectorThickness 4.09 mm
/Ge2SOS/det/setGeDetectorLength 17. mm
/Ge2SOS/det/setGeDetectorWidth 17. mm
/Ge2SOS/det/setCollimatorThickness 3. mm
#options for collimator material: titanium_grade1, titanium_grade2, tungsten_alloy, G4_Pb, G4_BRASS
/Ge2SOS/det/setCollimatorMat tungsten_alloy
/Ge2SOS/det/setDistanceSampleWindow 50. mm
/Ge2SOS/det/setBeWindowRadius 20.0 mm

#Source configuration
#Choise of source type: 0 gamma,1 Fe-55,2 Cs-137; 1 Bi-207; 2 Sr-90; 3 e-

/run/initialize
/Ge2SOS/gun/sourceType 0
/Ge2SOS/gun/sourceDiameter 0.3 mm
/Ge2SOS/gun/sourceEnergy 32.0  keV
/Ge2SOS/gun/sourceGammaPolarizationAngle 90.0
/Ge2SOS/gun/sourceGammaPolarizationDegree 0.99

/run/beamOn 900000000
```

The complete list of options can be found in the DetectorMessenger.cc and PrimaryGeneratorMessenger.cc, you can use as example if you want to include another option 

